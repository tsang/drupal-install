## Description ##
Drupal site installer with installation profile

## Deployment ##
First, get it downloaded and setup so you can call the installer from anywhere on your system:
Clone this repository to somewhere on your computer (e.g. ~/drush.make)
Create a symbolic link to the drupal-install file in a directory that's in your path, such as ~/bin. Make sure it has execute permission.

### Custom Configuration ###
There are going to be some configuration options that are the same every single time you run the installer, such as your local database username, password and the path to your make file. You can avoid typing these on the command line each time you run the installer by creating a custom configuration file to hold these values. It can be used to override any of the default options as well as set some additional installer options that cannot be set via command line parameters.

This file must reside in your home directory and be named .drupal-install.cfg. An example file comes with the installer script with examples and instructions (custom-config-example.cfg).

The options you will most commonly want to add/override are:
* dbuser - database username
* dbpass - database password
* makefile - path to the makefile

### Installer Assumptions ###
The installer makes some assumptions based on the way most of us do local development and acts accordingly. These assumptions can be overridden via the custom config file.

For one thing, it assumes that whatever directory you install the site in is going to be used as the fake hostname to access the site. Based on that, it will attempt to update your hosts file and add the directory as a hostname pointing to 127.0.0.1, if that host name is not already in the hosts file. After it's done that, it will attempt to run the drush uli command passing in the directory name as the URI, to cause it to automatically try and open the one-time login URL in your browser.

If you have to setup host names in some special way before they can work, you can disable the automatic hosts file updating via the custom config file. See the example for details.

It also by default assumes that the "_www" group is used by apache and the files should belong to that group and that the group should have write permissions. If that's not the case for you, the custom config file can be used to prevent it from setting permissions, or define a different username:groupname to apply.

### Running the Installer

To get help on all the supported parameters, type the command without any parameters:

```
#!bash

% drupal-install
```

If you did not modify any of the default options, then all of the following parameters need to be provided:

* dbname - database name
* dbuser - database username
* dbpass - database password
* siteuser - Drupal admin username
* sitepass - Drupal admin password
* sitemail - Drupal site config email address

A good custom config setup will provide all but the database name and site admin password, such that a command like this would be all you'd need to install the site:

```
#!bash

% drupal-install --dbname=dp_uphere --sitename="Uphere Magazine" --sitepass=password123
```

The site name of course is not required, but it saves time having to edit the site config later on.

During the installation you will be prompted for interaction at least once, at the point when it requires confirmation to create/replace the database and create the settings file and such:

```
#!bash

You are about to create a sites/default/files directory and create a sites/default/settings.php file and DROP all tables in your 'dp_uphere' database. Do you want to continue? (y/n):
```

If you have not recently used the sudo command, you will also be prompted for your password at the point when it tries to set permissions (based on your default config options where the user:group is defined).

That's pretty much it. If the installation was successful and everything went smoothly, then upon completion of the installation your browser should come to the front and open to the one-time login link provided by the drush uli command. You'll be logged in and ready to start developing.

## Note on Admin Usernames

We typically stick with "admin" as the username for user 1. This is generally not a very good practice as it's the most commonly used super user name. As such, please go with "[something]-admin" as the admin username, where [something] might be the client name abbreviation, for example.