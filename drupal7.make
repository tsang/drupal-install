;drupal 7 standard make file

core = 7.x

api = 2


; Projects
; --------
; Each project that you would like to include in the makefile should be
; declared under the `projects` key. The simplest declaration of a project
; looks like this:

projects[] = drupal

defaults[projects][subdir] = contrib

projects[] = views

projects[] = ctools

projects[] = date

projects[] = advanced_help

projects[] = libraries

projects[] = features

projects[] = strongarm

projects[] = pathauto

projects[] = token

projects[] = views_slideshow

projects[] = honeypot

projects[devel][subdir] = development

projects[] = diff

projects[] = devel_themer

projects[] = nice_menus

projects[] = transliteration

projects[] = colorbox

projects[] = pathologic

projects[] = insert

projects[] = menu_block

projects[] = publish_button

projects[] = entityreference

projects[] = entity

projects[] = admin_menu

projects[] = views_bulk_operations

projects[] = admin_views

projects[] = ckeditor

projects[] = menu_admin_per_menu

projects[] = media

projects[] = media_ckeditor

projects[] = file_entity

projects[] = panels

projects[] = field_group

projects[] = ccl

projects[] = link

projects[] = linkit

;projects[] = imagecrop

projects[] = smartcrop

projects[] = imagefield_focus

projects[] = google_analytics

projects[] = module_filter

projects[] = smart_trim

projects[] = taxonomy_access_fix

projects[] = user_settings_access

projects[] = custom_search

; Download the adminimal theme but don't put it in the contrib folder
projects[adminimal_theme][subdir] = ""

projects[] = 'adminimal_admin_menu'

; Download Zen theme, but don't put it in the default "contrib" directory
projects[zen][subdir] = ""
; Make sure we download the specific version of Zen that our base theme is built on
projects[zen][version] = 5.5

; CKEditor
libraries[ckeditor][download][type]= "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.8/ckeditor_4.5.8_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

; CKEditor plugins for use with media_ckeditor
libraries[ckeditor.lineutils][download][type]= "get"
libraries[ckeditor.lineutils][download][url] = "http://download.ckeditor.com/lineutils/releases/lineutils_4.5.8.zip"
libraries[ckeditor.lineutils][directory_name] = "lineutils"
libraries[ckeditor.lineutils][destination] = "libraries/ckeditor/plugins"

libraries[ckeditor.widget][download][type]= "get"
libraries[ckeditor.widget][download][url] = "http://download.ckeditor.com/widget/releases/widget_4.5.8.zip"
libraries[ckeditor.widget][directory_name] = "widget"
libraries[ckeditor.widget][destination] = "libraries/ckeditor/plugins"

; jquery cycle for views_slideshow
libraries[jquery.cycle][download][type]= "get"
libraries[jquery.cycle][download][url] = "http://malsup.github.com/jquery.cycle.all.js"
libraries[jquery.cycle][directory_name] = "jquery.cycle"
libraries[jquery.cycle][destination] = "libraries"

; jQuery colorbox plugin
libraries[jquery.colorbox][download][type] = "get"
libraries[jquery.colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"
libraries[jquery.colorbox][directory_name] = "colorbox"
libraries[jquery.colorbox][destination] = "libraries"

projects[kc_general_profile][subdir] = ""
projects[kc_general_profile][type] = 'profile'
projects[kc_general_profile][download][type] = 'git'
projects[kc_general_profile][download][url] = "git@bitbucket.org:tsang/drupal-general-profile.git"

projects[kci_base_theme][subdir] = ""
projects[kci_base_theme][type] = 'theme'
projects[kci_base_theme][download][url] = "git@bitbucket.org:tsang/my-base-theme.git"
projects[kci_base_theme][download][type] = 'git'
